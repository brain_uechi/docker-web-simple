# docker-web-simple

php-fpm + mysql + nginx


## 操作
### イメージのビルド
`docker-compose build`

### 起動
`docker-compose up`

### バックグラウンドで起動
`docker-compose up -d`

### 停止
`docker-compose stop`

### コンテナの廃棄
`docker-compose down`

### ログ
`docker-compose logs -f`


## directories
- _docker/  
	docker関連ファイル
- htdocs/  
	ドキュメントルート

## setting

### nginx
_docker/nginx/nginx.conf

### php.ini
_docker/php-fpm/php.ini

### mysql
_docker/mysql/conf.d/custom.cnf (編集後イメージのビルドが必要です)

`docker-conpose build mysql`

## mysql について
db_local という名前のデータベースが作成済みです。

`http://[docker-ip]/adminer` にて [adminer](https://www.adminer.org/) が利用できます。

### adminer 接続情報

```
サーバ: mysql
ユーザ名: db_local
パスワード: db_local
データベース: db_local
```
